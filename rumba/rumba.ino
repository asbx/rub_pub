#include <Wire.h>
#include <Zumo32U4.h>

Zumo32U4LineSensors lineSensors; // instance of the sensor
Zumo32U4LCD lcd; 
Zumo32U4Motors motors;

#define NUM_SENSORS 5
#define NUM_SENSERS_AREYS 10

uint16_t lineSensorValues[NUM_SENSERS_AREYS][NUM_SENSORS];
uint16_t maxlineSensorValues[NUM_SENSORS];
uint16_t minlineSensorValues[NUM_SENSORS];
uint16_t genlineSensorValues[NUM_SENSORS];

int buttonA = 14;
// spede is speed
int spede = 150;
int line_sens_count = 0; //QTR_EMITTERS 
bool ser_print = true; //Print to several monitor if true if false that do not print to serial monitor mainly for debug purposes

int kalibar(uint16_t *senser, uint16_t *min, uint16_t *max,bool qtr_emitters)
{
	for(int i = 0; i < NUM_SENSORS; i++)
	{
		min[i] = genlineSensorValues[i];
		max[i] = genlineSensorValues[i];
	}
	while(digitalRead(buttonA))
	{
		if(ser_print)
		{
			Serial.println("-------");
			print_list(min);
			print_list(max);
		}
		lineSensors.read(senser, qtr_emitters? QTR_EMITTERS_ON : QTR_EMITTERS_OFF);
		for(int i = 0;i< NUM_SENSORS;i++)
		{
			if(senser[i] < min[i])
				min[i] = senser[i];
			else if(senser[i] > max[i])
				max[i] = senser[i];
		}
	}
	return 0;
}

int kalibar_arveds(uint16_t *senser, uint16_t *min, uint16_t *max, bool qtr_emitters)
{
	//Initialize sensor values min and Max so they don't get preset from the initial values
	for(int i = 0; i < NUM_SENSORS; i++)
	{
		min[i] = genlineSensorValues[i];
		max[i] = genlineSensorValues[i];
	}
	while(digitalRead(buttonA))
	{
		linesens_matrix_update(&lineSensors, (uint16_t *)lineSensorValues, &line_sens_count, NUM_SENSERS_AREYS,NUM_SENSORS,qtr_emitters);
		//print_list(&(lineSensorValues[line_sens_count][0]));
		average_line_senser((uint16_t*)lineSensorValues, genlineSensorValues ,NUM_SENSERS_AREYS,NUM_SENSORS);
		if(ser_print)
			print_list(genlineSensorValues);
		for(int i = 0;i< NUM_SENSORS;i++)
		{
			if(genlineSensorValues[i] < min[i])
				min[i] = genlineSensorValues[i];
			else if(genlineSensorValues[i] > max[i])
				max[i] = genlineSensorValues[i];
		}
	}
	return 0;
}

void normel_lise(uint16_t *input,uint16_t *output, uint16_t *min, uint16_t *max, uint16_t rang_min , uint16_t rang_max, uint16_t antal)
{
	for(int i = 0;i < antal; i++)
	{
		output[i] = map(input[i],min[i],max[i],rang_min,rang_max);
	}
}

void average_line_senser(int *matrix, int *output, int areycount, int sensercount)
{
	long h = 0;
	for(int i = 0; i < sensercount; i++)
	{
		h = 0;
		for(int i2 = 0; i2 < areycount && (matrix + i2*sensercount)[i] != '\0' ; i2++ ) //There's a making additional 0 bity by check in the full loop so it ensures that the stop bite isn't overwritten
			h +=  (matrix + i2*sensercount)[i];
		output[i] =  h/areycount;
	}
}


void linesens_matrix_update(Zumo32U4LineSensors *li_sens, int *matrix , int *cound, int areycount, int sensercount,bool qtr_emitters )
{
	li_sens->read(( matrix + (*cound) * sensercount), qtr_emitters ? QTR_EMITTERS_ON : QTR_EMITTERS_OFF);
	(*cound)++;	
	if(*cound >= areycount)
		*cound = 0;
}

//Check if any value in the given array is under the threshold
bool all_over_freshol(int *list,int threshol,int length )
{
	bool h = true;
	for(int i = 0; i < length ; i++)
	{
		h = h && (list[i] < threshol);
	}
	return h;
}

//Print a specified array in serial plotter format
void print_list(int *list)
{
	String h[NUM_SENSORS] = {"r:","rc:","c:","cl:","l:"};
	for(int i = 0; i < NUM_SENSORS;i++)
	{
		Serial.print(h[i]);
		Serial.print(list[i % NUM_SENSORS]);
		Serial.print("  ");
	}
	Serial.println("uT");
}

void setup()
{
	Serial.begin(9600);
	lineSensors.initFiveSensors();
	lcd.clear();
	if(ser_print)
		Serial.println("tryk paa kanp for at stop");
	lcd.print("tryk på a");
	kalibar_arveds((int*)lineSensorValues,minlineSensorValues,maxlineSensorValues, true);
	lcd.clear();
	for(int i = 0; i < NUM_SENSORS && ser_print; i++)
	{
		Serial.println(i);
		Serial.print("max:");
		Serial.println(maxlineSensorValues[i]);
		Serial.print("min:");
		Serial.println(minlineSensorValues[i]);
		Serial.println("----------------");  
	}
}
void loop()
{ 
	//Drive forward until rich is rich
	motors.setSpeeds(spede,spede);
	do
	{
		linesens_matrix_update(&lineSensors, (uint16_t *)lineSensorValues, &line_sens_count, NUM_SENSERS_AREYS,NUM_SENSORS,true);
		average_line_senser((uint16_t*)lineSensorValues, genlineSensorValues ,NUM_SENSERS_AREYS,NUM_SENSORS);
		normel_lise(genlineSensorValues, genlineSensorValues, minlineSensorValues , maxlineSensorValues , 0 , 10000 , NUM_SENSORS);
		if(ser_print)
			print_list(genlineSensorValues);
	}while(all_over_freshol(genlineSensorValues, 6000, NUM_SENSORS ));
	
	//Drive backwards and then take a random angle 
	motors.setSpeeds(-spede,-spede);
	delay(100);
	motors.setSpeeds(spede,-spede);
	delay(rand() % 1000);
	
	//Override/(update all) rolling average from line sensor matrix
	for(int i = 0; i < NUM_SENSERS_AREYS; i++)
		linesens_matrix_update(&lineSensors, (uint16_t *)lineSensorValues, &line_sens_count, NUM_SENSERS_AREYS,NUM_SENSORS,true);
}
